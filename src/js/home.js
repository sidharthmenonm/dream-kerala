if ('serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('../service-worker.js');
  });
}

document.querySelector('.readmore').addEventListener('click', function(e) {
  e.preventDefault();
  if (document.querySelector('#main-section').classList.contains('read')) {
    document.querySelector('#main-section').classList.remove('read');
  } else {
    document.querySelector('#main-section').classList.add('read');
  }
});