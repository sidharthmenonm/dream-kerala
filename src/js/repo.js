import Vue from 'vue';
import axios from 'axios';

const app = new Vue({
  el: "#app",
  data: {
    resolutions: [],
    url: process.env.URL,
    api_url: process.env.API_URL,
  },
  mounted() {
    var _this = this;
    axios.get(this.api_url + '/resolutions').then(function(response) {
      console.log(response.data.resolutions);
      _this.resolutions = response.data.resolutions;
    });
  }
});